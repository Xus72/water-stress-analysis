from pyhdf.SD import SD, SDC
import os
import csv

#Datos meteorologicos
#Almonte
almonte_meteo = 'Data/Datos_climaticos/Almonte.csv'
moguer_meteo = 'Data/Datos_climaticos/Moguer.csv'

#LST MODIS
moda_lst = 'Data/MODA11_LST/'

#Lectura datos meteorologicos
def meteo_data(meteo):
    meteo_data = list()
    with open(meteo,'r') as f:
        csv_reader = csv.reader(f, delimiter=';')
        meteo_data = [row for row in csv_reader]
        f.close()
    meteo_clean = clean_data(meteo_data)
    return meteo_clean[:0:-1]

#Función para eliminar caracteres no deseados
def clean_data(meteo):
    meteo_data = list()
    for i in range(1, len(meteo)):
        for j in range(1, len(meteo[i])):
            if meteo[i][j] == '--.--':
                meteo[i][j] = '0.0'
            meteo_data.append(meteo[i][j])
    return meteo_data

#Lectura LST MODIS TERRA
def modis_terra_data(moda):
    moda_lst_data = list()
    moda_qa_data = list()
    moda_time_data = list()
    for root, dir, files in os.walk(moda_lst):
        moda_files = [moda_lst + filenames for filenames in files]
        for i in range(len(moda_files)):
            moda_hdf = SD(moda_files[i], SDC.READ)
            moda_lst_data.append(moda_hdf.select('LST_Day_1km')[270:375, 450:607])
            moda_qa_data.append(moda_hdf.select('QC_Day')[270:375, 450:607])
            moda_time_data.append(moda_hdf.select('Day_view_time')[270:375, 450:607])
    return moda_lst_data, moda_qa_data, moda_time_data

meteo = load_meteo_data(almonte_meteo)
print(meteo)